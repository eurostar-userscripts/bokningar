#![Bokningar](https://i.imgur.com/HAm9bJR.png)

*** 

På grund av att eurostar.se har text som är bilder så läser detta skript url-strängar för att få ut rätt värden av bokade säten. Värdena visas färgkodade med grön/gul/röd bakgrund. 0-40 = grön, 40-90 = gul, 90+ = röd.