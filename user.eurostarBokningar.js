// ==UserScript==
// @description This script will display number of booked seats on Eurostar.se, and color code for amount of crowd.
// @name Eurostar.se - Bokningar
// @author Victor Evertsson Heijler
// @version 1.4
// @include http://eurostar.se/html/platser.php*
// @include http://www.eurostar.se/html/platser.php*
// @updateURL http://heijler.se/eurostar/user.eurostarBokningar.js
// @run-at document-end
// @grant none
// ==/UserScript==

// The values for seats/booked seats are stored as images, retrieve all images.
var imageElements = document.querySelectorAll("img");
var arr = [];

// Retrieve 44th character of src of the images.
function getVal(x) {
    if ( window.location.host.length === 15 ) { //quick fix if www or not.
        return x.src.substr(48, 1);
    } else {
        return x.src.substr(44, 1);
    }
}

// Loop over all images, find every image with a number value (or v which is the divider)on 44th character and push value to array. 
for(var i = 0; i < imageElements.length; i++){
	if(Number.isInteger(parseInt(getVal(imageElements[i])), 10) || getVal(imageElements[i]) == "v") {
		arr.push(getVal(imageElements[i]));
	}
}

// Find the divider and split the array into two strings.
var separator = arr.indexOf("v");
var bookedSeats  = arr.slice(0, separator).join("");
var totalSeats = arr.slice(separator + 1).join("");
var result = totalSeats - bookedSeats;
var severity;

if(result >= 0 && result <= 40) {
	severity = "hsla(138, 100%, 50%, 0.3)"; // Green
} else if (result > 40 && result <= 90) {
	severity = "hsla(53, 100%, 50%, 0.3)";  // Yellow
} else {
	severity = "hsla(0, 100%, 50%, 0.3)";   // Red
}

// Add html and style to display the value of result, with color coding depending on how many booked.
var div = document.createElement("div");
div.innerHTML = "<p class='antal-bokningar' style='font-size:13px;position:absolute;top:15px;left:25px;padding:15px;background:" + severity + ";border-radius:4px;'>Bokade platser: " + result + "</p>";
document.body.appendChild(div);

// Set background all black, looks a bit better
document.body.background = "";